include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)

add_definitions(
  -DGETTEXT_PACKAGE=\"${PROJECT_NAME}\"
  -DGETTEXT_LOCALEDIR=\"${CMAKE_INSTALL_FULL_LOCALEDIR}\"
  -DQT_NO_KEYWORDS
)

pkg_check_modules(GEONAMES REQUIRED geonames)

set(
    alarm_SRCS
    modules/Alarm/backend.cpp
    modules/Alarm/settings.cpp
    modules/Alarm/sound.cpp
    modules/Alarm/sortedalarmsmodel.cpp
)

set(
    worldclock_SRCS
    modules/WorldClock/backend.cpp
    modules/WorldClock/geolocation.cpp
    modules/WorldClock/datetime.cpp
    modules/WorldClock/timezonemodel.cpp
    modules/WorldClock/generictimezonemodel.cpp
    modules/WorldClock/geonamestimezonemodel.cpp
    modules/WorldClock/statictimezonemodel.cpp
)

set(
    stopwatch_SRCS
    modules/Stopwatch/backend.cpp
    modules/Stopwatch/engine.cpp
    modules/Stopwatch/formattime.cpp
)

add_library(alarm MODULE
    ${alarm_SRCS}
)

add_library(worldclock MODULE
    ${worldclock_SRCS}
)

add_library(stopwatch MODULE
    ${stopwatch_SRCS}
)

set_target_properties(alarm PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY ClockApp/Alarm
)

set_target_properties(worldclock PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY ClockApp/WorldClock
)

set_target_properties(stopwatch PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY ClockApp/Stopwatch
)

target_link_libraries(alarm PUBLIC Qt5::Gui Qt5::Qml Qt5::Quick Qt5::DBus)

target_include_directories(worldclock PUBLIC ${GEONAMES_INCLUDE_DIRS})
target_link_libraries(worldclock PUBLIC ${GEONAMES_LDFLAGS} Qt5::Gui Qt5::Qml Qt5::Quick)

target_link_libraries(stopwatch PUBLIC Qt5::Qml)

# Copy qmldir file to build dir for running in QtCreator
add_custom_target(alarm-qmldir ALL
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/modules/Alarm/qmldir ${CMAKE_CURRENT_BINARY_DIR}/ClockApp/Alarm
    DEPENDS ${QMLFILES}
)

add_custom_target(worldclock-qmldir ALL
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/modules/WorldClock/qmldir ${CMAKE_CURRENT_BINARY_DIR}/ClockApp/WorldClock
    DEPENDS ${QMLFILES}
)

add_custom_target(stopwatch-qmldir ALL
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/modules/Stopwatch/qmldir ${CMAKE_CURRENT_BINARY_DIR}/ClockApp/Stopwatch
    DEPENDS ${QMLFILES}
)

# Install plugin file
install(TARGETS alarm DESTINATION ${MODULE_PATH}/Alarm/)
install(FILES   modules/Alarm/qmldir DESTINATION ${MODULE_PATH}/Alarm/)

install(TARGETS worldclock DESTINATION ${MODULE_PATH}/WorldClock/)
install(FILES   modules/WorldClock/qmldir DESTINATION ${MODULE_PATH}/WorldClock/)

install(TARGETS stopwatch DESTINATION ${MODULE_PATH}/Stopwatch/)
install(FILES   modules/Stopwatch/qmldir DESTINATION ${MODULE_PATH}/Stopwatch/)
